# Afrikaans translation of PipeWire.
# This file is distributed under the same license as the PipeWire package.
# F Wolff <friedel@translate.org.za>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: master\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2019-01-09 12:17+0200\n"
"Last-Translator: F Wolff <friedel@translate.org.za>\n"
"Language-Team: translate-discuss-af@lists.sourceforge.net\n"
"Language: af\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr "Ingeboude oudio"

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr "Modem"

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr ""
